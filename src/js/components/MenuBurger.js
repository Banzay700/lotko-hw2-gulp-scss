const MenuBurger = () => {
   const page = document.documentElement
   const button = document.querySelector('.icon-menu')

   button.addEventListener('click', () => page.classList.toggle('menu-open'))
}

export default MenuBurger
